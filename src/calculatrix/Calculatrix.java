package calculatrix;

public class Calculatrix {
	
	static double resultat;

	public static double calculer(char operateur, double var1, double var2) {
		try {
			resultat = operateur == '+' ? 
					var1 + var2 : 
						operateur == '-' ? 
					var1 - var2 : 
						operateur == '*' ?
					var1 * var2 : 
						operateur == '/' ?
					var1 / var2 : null;
			System.out.printf("Résultat: " + var1 + " " + operateur + " " + var2 + " = " + resultat + "\n");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resultat;
	}
	
}
