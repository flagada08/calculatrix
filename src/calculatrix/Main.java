package calculatrix;

public class Main {

	public static void main(String[] args) {
		Calculatrix.calculer( '+', 1, 2 );
		Calculatrix.calculer( '-', 2078, 64 );
		Calculatrix.calculer( '*', 15, 3 );
		Calculatrix.calculer( '/', 42, 9 );
		Calculatrix.calculer( '+', Calculatrix.calculer('/', 15, 2), Calculatrix.calculer( '*', 2, 3 ));
	}

}
